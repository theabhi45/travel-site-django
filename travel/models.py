from django.db import models
import itertools
from django.utils.text import slugify


# Create your models here.

class Category(models.Model):
  name = models.CharField(max_length=200)
  slug = models.SlugField(unique = True, null=True, blank=True)
  description = models.CharField(max_length= 1000 ,null = True)


  class Meta:
    verbose_name = "category"
    verbose_name_plural = "categories"

  def __str__(self):
      return self.name
  
  def _generate_slug(self):
        max_length = self._meta.get_field('slug').max_length
        value = self.name
        slug_candidate = slug_original = slugify(value, allow_unicode=True)
        for i in itertools.count(1):
            if not Category.objects.filter(slug=slug_candidate).exists():
                break
            slug_candidate = '{}-{}'.format(slug_original, i)

        self.slug = slug_candidate

  def save(self, *args, **kwargs):
      if not self.pk:
          self._generate_slug()

      super().save(*args, **kwargs)

class Sub_Category(models.Model):
  name = models.CharField(max_length=200)
  category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True)
  description = models.CharField(max_length= 1000, null = True)

  class Meta:
    verbose_name = "sub_category"
    verbose_name_plural = "sub_categories"

  def __str__(self):
    return self.name

class Specific_Category(models.Model):
  name = models.CharField(max_length=200)
  sub_category = models.ForeignKey(Sub_Category, on_delete=models.SET_NULL, null=True)
  description = models.CharField(max_length= 1000, null = True)

  class Meta:
    verbose_name_plural = "specific_categories"

  def __str__(self):
    return self.name





  
