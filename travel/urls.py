from django.urls import path
from . import views
from .views import *

urlpatterns = [
    path('', views.home, name='home'),
    path('contact-us/',views.contacts , name="contact"),
    path('our-team/',views.teams, name= "team"),
    path('<str:slug>/',views.category , name="category"),
]