from django.shortcuts import render
from .models import *
# Create your views here.

def home(request):
  primary_categories = Category.objects.order_by('id')[:5]
  remaining_categories = Category.objects.order_by('id')[5:]
  
  context = {
    'categories':primary_categories, 
    'rem_categories':remaining_categories
    }
  return render(request,'home/home.html', context)

def category(request ,slug):
  primary_categories = Category.objects.order_by('id')[:5]
  remaining_categories = Category.objects.order_by('id')[5:]
  category = Category.objects.filter(slug=slug)
  context = {
    'categories':primary_categories, 
    'rem_categories':remaining_categories,
    'category' : category
    }
  return render(request, 'category.html',context)

def contacts(request):

  context = {}
  return render(request, 'contact_us.html',context)

def teams(request):
  context = {range:range(1,7)}
  return render(request, 'our_team.html',context)